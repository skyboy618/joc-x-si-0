public class InitializareJoc extends Xsi0 {
    public static void main(String[] args) {
        initJoc();
        do {
            miscareJucator(jucatorCurent); // actualizeaza linia si coloana curente
            actualizareJoc(jucatorCurent, linieCurenta, coloanaCurenta); // actualizeaza starea curenta a jocului
            afisareTabla();

            if (stareCurenta == xCastiga) {
                System.out.println("'X' castiga!");
            } else if (stareCurenta == zeroCastiga) {
                System.out.println("'O' castiga!");
            } else if (stareCurenta == egalitate) {
                System.out.println("EGALITATE");
            }
            // schimba jucator
            jucatorCurent = (jucatorCurent == x) ? zero : x;
        } while (stareCurenta == PLAYING); // repeta atat timp cat nu castiga nimeni/nu este egalitate
    }
}
