import java.util.*;
public class Xsi0 {

       //variabile ce reprezinta continutul celulelor
        public static final int EMPTY = 0;
        public static final int x = 1;
        public static final int zero = 2;

        // variabile ce reprezinta starile jocului
        public static final int PLAYING = 0;
        public static final int egalitate = 1;
        public static final int xCastiga = 2;
        public static final int zeroCastiga = 3;

        // tabla de joc si starea jocului
        public static final int linii = 3, coloane = 3; // numarul de linii si coloane
        public static int[][] tabla = new int[linii][coloane]; // tabla de joc - matrice

        public static int stareCurenta;  // starea curenta a jocului
        // (Playing, egalitate, x castiga, zero castiga)
        public static int jucatorCurent; // jucatorul curent (X sau 0)
        public static int linieCurenta, coloanaCurenta;

        public static Scanner scanner = new Scanner(System.in);


        public static void initJoc() {
            for (int linie = 0; linie < linii; ++linie) {
                for (int col = 0; col < coloane; ++col) {
                    tabla[linie][col] = EMPTY;  // toate celulele sunt goale
                }
            }
            stareCurenta = PLAYING;
            jucatorCurent = x;  // x incepe primul
        }

        public static void miscareJucator(int miscare) {
            boolean validInput = false;  // pentru a introduce date valide
            do {
                if (miscare == x) {
                    System.out.print("jucator 'X', introduceti miscarea (linie[1-3] coloana[1-3]): ");
                } else {
                    System.out.print("jucator 'O', introduceti miscarea (linie[1-3] coloana[1-3]): ");
                }
                int linie = scanner.nextInt() - 1;  // indexul incepe de la 1
                int col = scanner.nextInt() - 1;
                if (linie >= 0 && linie < linii && col >= 0 && col < coloane && tabla[linie][col] == EMPTY) {
                    linieCurenta = linie;
                    coloanaCurenta = col;
                    tabla[linieCurenta][coloanaCurenta] = miscare;  // actualizeaza continutul tablei
                    validInput = true;  //
                } else {
                    System.out.println("This move at (" + (linie + 1) + "," + (col + 1)
                            + ") is not valid. Try again...");
                }
            } while (!validInput);  // repeta pana cand jucatorul introduce date valide
        }

        public static void actualizareJoc(int miscare, int linieCurenta, int coloanaCurenta) {
            if (Castiga(miscare, linieCurenta, coloanaCurenta)) {  // verifica daca mutarea este castigatoare
                stareCurenta = (miscare == x) ? xCastiga : zeroCastiga;
            } else if (getEgalitate()) {  // verifica pentru egalitate
                stareCurenta = egalitate;
            }

        }

        public static boolean getEgalitate() {
            for (int linie = 0; linie < linii; ++linie) {
                for (int col = 0; col < coloane; ++col) {
                    if (tabla[linie][col] == EMPTY) {
                        return false;
                    }
                }
            }
            return true;  // nu mai sunt celule libere. egalitate
        }


        public static boolean Castiga(int miscare, int linieCurenta, int coloanaCurenta) {
            return (tabla[linieCurenta][0] == miscare         // 3 in linie
                    && tabla[linieCurenta][1] == miscare
                    && tabla[linieCurenta][2] == miscare
                    || tabla[0][coloanaCurenta] == miscare      // 3 pe coloana
                    && tabla[1][coloanaCurenta] == miscare
                    && tabla[2][coloanaCurenta] == miscare
                    || linieCurenta == coloanaCurenta            // 3 pe diagonala principala
                    && tabla[0][0] == miscare
                    && tabla[1][1] == miscare
                    && tabla[2][2] == miscare
                    || linieCurenta + coloanaCurenta == 2  // 3 pe diagonala secundara
                    && tabla[0][2] == miscare
                    && tabla[1][1] == miscare
                    && tabla[2][0] == miscare);
        }


        public static void afisareTabla() {
            for (int linie = 0; linie < linii; ++linie) {
                for (int col = 0; col < coloane; ++col) {
                    afisareCelula(tabla[linie][col]);
                    if (col != coloane - 1) {
                        System.out.print("|");
                    }
                }
                System.out.println();
                if (linie != linii - 1) {
                    System.out.println("-----------");
                }
            }
            System.out.println();
        }


        public static void afisareCelula(int continut) {
            switch (continut) {
                case EMPTY:  System.out.print("   "); break;
                case zero: System.out.print(" O "); break;
                case x:  System.out.print(" X "); break;
            }
        }
    }





